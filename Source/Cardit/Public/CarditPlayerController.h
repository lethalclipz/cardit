// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "CarditPlayerController.generated.h"

USTRUCT(BlueprintType)
struct FPlayerInfo
{
	GENERATED_USTRUCT_BODY()

	FPlayerInfo() {}

	UPROPERTY(BlueprintReadWrite)
	FString Username;
};


/**
 * 
 */
UCLASS()
class CARDIT_API ACarditPlayerController : public APlayerController
{
	GENERATED_BODY()
	
private:

	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;

public:

	UFUNCTION(Client, Reliable, WithValidation)
	void SetupIngameUI();
	void SetupIngameUI_Implementation();
	bool SetupIngameUI_Validate();

	UFUNCTION(BlueprintImplementableEvent)
	void DrawHitmarker();

	FPlayerInfo GetPlayerInfo();

protected:

	UPROPERTY(BlueprintReadWrite)
	bool bWantsToFire = false;

	UPROPERTY(EditAnywhere)
	FString Username = "DEFAULT_USERNAME";

	UFUNCTION(BlueprintImplementableEvent)
	void SetupIngameUI_Internal();
};
