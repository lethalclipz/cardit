// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "CarditCharacter.h" // For struct
#include "CarditWeapon.generated.h"



/**
 * 
 */
UCLASS(Blueprintable, meta=(BlueprintSpawnableComponent))
class CARDIT_API UCarditWeapon : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UCarditWeapon();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Fire();
	
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	int32 GetCurrentAmmoInMagazine() const { return AmmoInMagazine; }
	
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	int32 GetCurrentReserveAmmo() const { return ReserveAmmo; }

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void RequestReload();

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	bool IsReloading() const { return bIsReloading; }

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	float GetReloadProgress() const { if (GetWorld()->GetTimerManager().IsTimerActive(ReloadTimer)) { return GetWorld()->GetTimerManager().GetTimerElapsed(ReloadTimer) / ReloadTimeSeconds; } else { return 0.f; } }
	
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	float GetReloadTimeRemaining() const { if (GetWorld()->GetTimerManager().IsTimerActive(ReloadTimer)) { return GetWorld()->GetTimerManager().GetTimerRemaining(ReloadTimer); } else { return 0.f; } }

	UFUNCTION(Server, Reliable, WithValidation)
	void DamageCharacter(ACarditCharacter* CharacterToDamage, float Damage, const FPlayerInfo& PlayerInfo);
	void DamageCharacter_Implementation(ACarditCharacter* CharacterToDamage, float Damage, const FPlayerInfo& PlayerInfo);
	bool DamageCharacter_Validate(ACarditCharacter* CharacterToDamage, float Damage, const FPlayerInfo& PlayerInfo);

	UFUNCTION(BlueprintCallable, Client, Reliable, WithValidation, Category = "Ammo")
	void AddAmmo(int32 Amount);
	void AddAmmo_Implementation(int32 Amount);
	bool AddAmmo_Validate(int32 Amount);

private:

	int32 AmmoInMagazine;

	int32 ReserveAmmo;

	void Reload();

	float LastFireTime = 0.f;

	float GetTimeBetweenShotsSeconds() { return 60 / RPM; }

	bool bIsReloading = false;

	FTimerHandle ReloadTimer;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 MagazineSize = 30;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 StartingAmmo = 90;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float ReloadTimeSeconds = 3.f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxRangeInCm = 100000.f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int DamagePerShot = 20;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float HorizontalRecoilFactor = .5f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float VerticalRecoilFactor = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float RPM = 650.f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	USoundBase* WeaponSound = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	USoundAttenuation* SoundAttenuationSettings = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	UParticleSystem* GunshotExplosionParticleSystem = nullptr;
};
