// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "CarditPlayerController.h" // For struct
#include "CarditCharacter.generated.h"

class UInputComponent;
class UCarditWeapon;

UENUM(BlueprintType)
enum class ECharacterType : uint8
{
	Human,
	AI
};

USTRUCT()
struct FGunshotInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FVector Location;

	UPROPERTY()
	FVector ParticleEffectLocation;

	UPROPERTY()
	UParticleSystem* ParticleEffect;

	UPROPERTY()
	USoundBase* Sound;

	UPROPERTY()
	USoundAttenuation* SoundAttenuationSettings;

	UPROPERTY()
	float Volume = 1.f;

	UPROPERTY()
	float CallingCharacterID;
};

UCLASS(config = Game)
class ACarditCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ACarditCharacter();

	virtual void BeginPlay();

	virtual void Tick(float DeltaTime) override;

	bool bIsDestroyed = false;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void CharStartJumping();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void CharStopJumping();

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	UFUNCTION(BlueprintPure, Category = "Character Variables")
	int GetHealth() const { return Health; }

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	UFUNCTION(BlueprintCallable, Category = "Character Object Getters")
	UCameraComponent* GetCamera() const { return Camera; }

	UFUNCTION(BlueprintCallable, Category = "Character Object Getters")
	UStaticMeshComponent* GetCurrentFirstPersonWeaponMesh() const { return CurrentFirstPersonWeaponMesh; }

	UFUNCTION(BlueprintCallable, Category = "Character Object Getters")
	UCarditWeapon* GetCurrentThirdPersonWeapon() const { return CurrentThirdPersonWeapon; }

	ECharacterType GetCharacterType() const { return CharacterType; }

	float GetCharacterID() const { return CharacterID; }

	bool IsDamageable() const { return bIsDamageable; }

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "Damage")
	void DealDamage(int32 Damage, FPlayerInfo PlayerInfo);
	void DealDamage_Implementation(int32 Damage, FPlayerInfo PlayerInfo);
	bool DealDamage_Validate(int32 Damage, FPlayerInfo PlayerInfo);

	UFUNCTION(BlueprintImplementableEvent)
	void AddRecoil(float HorizontalFactor, float VerticalFactor);

	UFUNCTION(BlueprintImplementableEvent)
	void PlayFireMontage();

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void AttemptFireCurrentWeapon();

	UFUNCTION(Server, Reliable, WithValidation)
	void FireServerSide(const FGunshotInfo& GunshotInfo);
	void FireServerSide_Implementation(const FGunshotInfo& GunshotInfo);
	bool FireServerSide_Validate(const FGunshotInfo& GunshotInfo);

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void ClientFired(const FGunshotInfo& GunshotInfo);
	void ClientFired_Implementation(const FGunshotInfo& GunshotInfo);
	bool ClientFired_Validate(const FGunshotInfo& GunshotInfo);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	virtual void BeginDestroy() override;

	float CharacterID;

	UPROPERTY(EditDefaultsOnly, Replicated, Category = "Setup")
	int StartingHealth = 100;

	UPROPERTY(VisibleAnywhere, Replicated, Category = "Setup")
	int Health;

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void InitializeHuman(UCameraComponent* CameraToSet, UStaticMeshComponent* FirstPersonWeaponMeshToSet, UCarditWeapon* ThirdPersonWeaponToSet, USkeletalMeshComponent* MeshToSet, UCapsuleComponent* CapsuleToSet);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void InitializeAI(USkeletalMeshComponent* MeshToSet, UCapsuleComponent* CapsuleToSet, USceneComponent* HealthWidgetToSet);

	UPROPERTY(EditDefaultsOnly)
	ECharacterType CharacterType = ECharacterType::AI;

	UCameraComponent* Camera = nullptr;
	UStaticMeshComponent* CurrentFirstPersonWeaponMesh = nullptr;
	UCarditWeapon* CurrentThirdPersonWeapon = nullptr;
	USkeletalMeshComponent* SkeletalMesh = nullptr;
	UCapsuleComponent* Capsule = nullptr;
	USceneComponent* HealthWidget = nullptr;

	UPROPERTY(BlueprintReadOnly)
	bool bIsDamageable = true;

	UPROPERTY(BlueprintReadOnly)
	bool bIsDead = false;

	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* FiringAnimationMontage = nullptr;

	UPROPERTY(EditDefaultsOnly)
	TArray<UAnimationAsset*> DeathAnimations;

};

