// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CarditCharacter.h"
#include "CarditAICharacter.generated.h"

/**
 * 
 */
UCLASS()
class CARDIT_API ACarditAICharacter : public ACarditCharacter
{
	GENERATED_BODY()

public: 

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Killed(FPlayerInfo PlayerInfo);
	void Killed_Implementation(FPlayerInfo PlayerInfo);
	bool Killed_Validate(FPlayerInfo PlayerInfo);

protected:

	UFUNCTION(BlueprintImplementableEvent)
	void SetCharacterReferenceForHealthWidget();

	UFUNCTION(BlueprintImplementableEvent)
	void BPKilled();

private:

	ACarditAICharacter();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	void DestroyDelegate();

	bool bShouldManuallySetLocationAndRotation = false;

	FVector ManualLocationToSet;
	FRotator ManualRotationToSet;
};
