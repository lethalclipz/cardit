// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "CarditAIController.generated.h"

class ACarditCharacter;

/**
 * 
 */
UCLASS()
class CARDIT_API ACarditAIController : public AAIController
{
	GENERATED_BODY()

public:

	bool bShouldFollowCharacter = true;
	
private:

	virtual void Tick(float DeltaTime) override;

	void UpdateCurrentCharacter();
	float LastCharacterUpdateCallTime = -999.f;
	
	ACarditCharacter* CurrentCharacter = nullptr;
};
