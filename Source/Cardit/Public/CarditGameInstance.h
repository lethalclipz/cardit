// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "CarditGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CARDIT_API UCarditGameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void Init() override;

public:

	void SetUsername(FString Username);
	
private:

	FString Section = "General";
};
