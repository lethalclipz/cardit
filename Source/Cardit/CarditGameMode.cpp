// Fill out your copyright notice in the Description page of Project Settings.

#include "Cardit.h"
#include "CarditGameMode.h"
#include "UnrealNetwork.h"
#include "CarditPlayerController.h"
#include "CarditCharacter.h"
#include "CarditAICharacter.h"

ACarditGameMode::ACarditGameMode()
{
	SetBandwidthLimit(5000000);
}

void ACarditGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if (IsAGameplayMap(GetWorld()->GetMapName()))
	{
		if (Cast<ACarditPlayerController>(NewPlayer))
		{
			Cast<ACarditPlayerController>(NewPlayer)->SetupIngameUI();
			NewPlayer->ClientMessage(TEXT("Hello!"), FName(NAME_None), 5.f);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *GetWorld()->GetMapName());
	}
}

bool ACarditGameMode::IsAGameplayMap(FString MapName)
{
	TArray<FString> GameplayMaps;

	GameplayMaps.Add(FString("TemporaryLayoutMap"));
	GameplayMaps.Add(FString("UEDPIE_0_TemporaryLayoutMap"));
	GameplayMaps.Add(FString("UEDPIE_1_TemporaryLayoutMap"));

	return GameplayMaps.Contains(MapName);
}