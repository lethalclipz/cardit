// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "CarditGameMode.generated.h"

class ACarditCharacter;
class ACarditAICharacter;

/**
 * 
 */
UCLASS()
class CARDIT_API ACarditGameMode : public AGameMode
{
	GENERATED_BODY()
	
private:

	ACarditGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;
	
	bool IsAGameplayMap(FString MapName);
};
