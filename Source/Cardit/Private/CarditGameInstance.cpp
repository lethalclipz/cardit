// Fill out your copyright notice in the Description page of Project Settings.

#include "Cardit.h"
#include "CarditGameInstance.h"

void UCarditGameInstance::Init()
{
	Super::Init();

	UE_LOG(LogTemp, Warning, TEXT("Instance Init"));

	FString UsernameReceived;
	GConfig->GetString(
		*Section,
		TEXT("Username"),
		UsernameReceived,
		GGameIni
	);

	if (!UsernameReceived.IsEmpty())
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *UsernameReceived);
	}
	else
	{
		FString DefaultUsername = "Player" + FString::FromInt(FMath::Rand()) + FString::FromInt(FMath::Rand()) + FString::FromInt(FMath::Rand());
		SetUsername(DefaultUsername);
	}
}

void UCarditGameInstance::SetUsername(FString Username)
{
	GConfig->SetString(
		*Section,
		TEXT("Username"),
		*Username,
		GGameIni
	);
	GConfig->Flush(false, GGameIni);
}
