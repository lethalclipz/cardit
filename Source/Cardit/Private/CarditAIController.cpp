// Fill out your copyright notice in the Description page of Project Settings.

#include "Cardit.h"
#include "CarditCharacter.h"
#include "CarditAIController.h"

void ACarditAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Only look for new closest player every 500ms for performance reasons
	if (GetWorld()->GetTimeSeconds() - LastCharacterUpdateCallTime >= .500 && bShouldFollowCharacter)
	{
		UpdateCurrentCharacter();
		LastCharacterUpdateCallTime = GetWorld()->GetTimeSeconds();
	}

	if (bShouldFollowCharacter)
	{
		MoveToActor(CurrentCharacter);
	}
	else
	{
		StopMovement();
	}
}

void ACarditAIController::UpdateCurrentCharacter()
{
	TArray<UObject*> Objects = TArray<UObject*>();
	GetObjectsOfClass(ACarditCharacter::StaticClass(), Objects);

	ACarditCharacter* ClosestCharacter = nullptr;

	UNavigationSystem* NavigationSystem = GetWorld()->GetNavigationSystem();

	for (UObject* Object : Objects)
	{
		if (Cast<ACarditCharacter>(Object))
		{
			if (Cast<ACarditCharacter>(Object)->GetCharacterType() == ECharacterType::Human)
			{
				ACarditCharacter* MyCharacter = Cast<ACarditCharacter>(GetPawn());
				ACarditCharacter* NewCharacter = Cast<ACarditCharacter>(Object);

				if (!ClosestCharacter)
				{
					ClosestCharacter = NewCharacter;
				}
				else
				{
					float DistanceToClosestCharacter = (MyCharacter->GetActorLocation() - ClosestCharacter->GetActorLocation()).Size();
					float DistanceToNewCharacter = (MyCharacter->GetActorLocation() - NewCharacter->GetActorLocation()).Size();

					if (DistanceToNewCharacter < DistanceToClosestCharacter)
					{
						ClosestCharacter = NewCharacter;
					}
					/* Bases calculation on "Path Cost" rather than distance, not quite working
					float NewCharacterCost = 0.f, CurrentClosestCharacterCost = 0.f;

					NavigationSystem->GetPathCost(GetPawn()->GetActorLocation(), Cast<ACarditCharacter>(Object)->GetActorLocation(), NewCharacterCost);
					NavigationSystem->GetPathCost(GetPawn()->GetActorLocation(), ClosestCharacter->GetActorLocation(), CurrentClosestCharacterCost);

					UE_LOG(LogTemp, Warning, TEXT("ClosestCharacterLength: %f, NewCharacterLength: %f"), CurrentClosestCharacterCost, NewCharacterCost);

					if (NewCharacterCost < CurrentClosestCharacterCost)
					{
						ClosestCharacter = Cast<ACarditCharacter>(Object);
					}*/
				}
			}
		}
	}

	CurrentCharacter = ClosestCharacter;
}