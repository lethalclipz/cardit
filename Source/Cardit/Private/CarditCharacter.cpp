// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Cardit.h"
#include "CarditCharacter.h"
#include "CarditAICharacter.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "UnrealNetwork.h"
#include "CarditWeapon.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ATempCPPShooterCharacter

ACarditCharacter::ACarditCharacter()
{
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
}

void ACarditCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	Health = StartingHealth;

	CharacterID = FMath::FRand() * 1000000;
}

void ACarditCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACarditCharacter::InitializeHuman(UCameraComponent* CameraToSet, UStaticMeshComponent* FirstPersonWeaponMeshToSet, UCarditWeapon* ThirdPersonWeaponToSet, USkeletalMeshComponent* MeshToSet, UCapsuleComponent* CapsuleToSet)
{
	Camera = CameraToSet;
	CurrentFirstPersonWeaponMesh = FirstPersonWeaponMeshToSet;
	CurrentThirdPersonWeapon = ThirdPersonWeaponToSet;
	SkeletalMesh = MeshToSet;
	Capsule = CapsuleToSet;
}

void ACarditCharacter::InitializeAI(USkeletalMeshComponent* MeshToSet, UCapsuleComponent* CapsuleToSet, USceneComponent* HealthWidgetToSet)
{
	SkeletalMesh = MeshToSet;
	Capsule = CapsuleToSet;
	HealthWidget = HealthWidgetToSet;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ACarditCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	
}

void ACarditCharacter::BeginDestroy()
{
	Super::BeginDestroy();
}

void ACarditCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ACarditCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ACarditCharacter::CharStartJumping()
{
	Jump();
}

void ACarditCharacter::CharStopJumping()
{
	StopJumping();
}

void ACarditCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACarditCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ACarditCharacter::DealDamage_Implementation(int32 Damage, FPlayerInfo PlayerInfo)
{
	if (bIsDamageable)
	{
		Health = FMath::Clamp(Health - Damage, 0, StartingHealth);

		if (Health <= 0)
		{
			if (this->GetCharacterType() == ECharacterType::AI)
			{
				Cast<ACarditAICharacter>(this)->Killed(PlayerInfo);
			}
			else if (this->GetCharacterType() == ECharacterType::Human)
			{
				/* Killed Method */ bIsDamageable = false;
				Cast<APlayerController>(GetController())->StartSpectatingOnly();
			}
		}
	}
}

bool ACarditCharacter::DealDamage_Validate(int32 Damage, FPlayerInfo PlayerInfo)
{
	return true;
}

void ACarditCharacter::AttemptFireCurrentWeapon()
{
	CurrentThirdPersonWeapon->Fire();
}

void ACarditCharacter::FireServerSide_Implementation(const FGunshotInfo& GunshotInfo)
{
	ClientFired(GunshotInfo);
}

bool ACarditCharacter::FireServerSide_Validate(const FGunshotInfo& GunshotInfo)
{
	return true;
}

void ACarditCharacter::ClientFired_Implementation(const FGunshotInfo& GunshotInfo)
{
	if (GunshotInfo.CallingCharacterID != CharacterID)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), GunshotInfo.Sound, GunshotInfo.Location, GunshotInfo.Volume, 1.f, 0.f, GunshotInfo.SoundAttenuationSettings);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GunshotInfo.ParticleEffect, GunshotInfo.ParticleEffectLocation);
		if (FiringAnimationMontage)
		{
			SkeletalMesh->GetAnimInstance()->Montage_Play(FiringAnimationMontage);
		}
	}
}

bool ACarditCharacter::ClientFired_Validate(const FGunshotInfo& GunshotInfo)
{
	return true;
}

void ACarditCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACarditCharacter, Health);
}

