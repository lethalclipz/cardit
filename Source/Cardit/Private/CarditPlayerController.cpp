// Fill out your copyright notice in the Description page of Project Settings.

#include "Cardit.h"
#include "CarditPlayerController.h"
#include "UnrealNetwork.h"
#include "CarditCharacter.h"
#include "CarditWeapon.h"

void ACarditPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bWantsToFire)
	{
		Cast<ACarditCharacter>(GetPawn())->AttemptFireCurrentWeapon();
	}
}

void ACarditPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ACarditPlayerController::SetupIngameUI_Implementation()
{
	SetupIngameUI_Internal();
}

bool ACarditPlayerController::SetupIngameUI_Validate()
{
	return true;
}

FPlayerInfo ACarditPlayerController::GetPlayerInfo()
{
	FPlayerInfo PlayerInfo;

	PlayerInfo.Username = Username;

	return PlayerInfo;
}
