// Fill out your copyright notice in the Description page of Project Settings.

#include "Cardit.h"
#include "UnrealNetwork.h"
#include "CarditAIController.h"
#include "CarditAICharacter.h"

ACarditAICharacter::ACarditAICharacter()
{
	GetMesh()->bEnablePhysicsOnDedicatedServer = true;
}

void ACarditAICharacter::BeginPlay()
{
	Super::BeginPlay();

	SetCharacterReferenceForHealthWidget();
}

void ACarditAICharacter::Tick(float DeltaTime)
{
	if (bShouldManuallySetLocationAndRotation)
	{
		SetActorLocationAndRotation(ManualLocationToSet, ManualRotationToSet);
	}
}

void ACarditAICharacter::Killed_Implementation(FPlayerInfo PlayerInfo)
{
	BPKilled();

	bIsDamageable = false;
	bIsDead = true;

	if (HasAuthority())
	{
		// Redundancy check
		if (GetController())
		{
			Cast<ACarditAIController>(GetController())->bShouldFollowCharacter = false;
			Cast<ACarditAIController>(GetController())->StopMovement();
			DetachFromControllerPendingDestroy();
		}
	}

	HealthWidget->DestroyComponent(false);

	// Play random death animation
	if (DeathAnimations.Num() > 0)
	{
		int32 IndexOfAnimationToPlay = FMath::RandRange(0, DeathAnimations.Num() - 1);
		SkeletalMesh->PlayAnimation(DeathAnimations[IndexOfAnimationToPlay], false);
	}

	Capsule->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	Capsule->bGenerateOverlapEvents = false;
	SkeletalMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	SkeletalMesh->bGenerateOverlapEvents = false;

	ManualLocationToSet = GetActorLocation();
	ManualRotationToSet = GetActorRotation();
	bShouldManuallySetLocationAndRotation = true;

	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(Timer, this, &ACarditAICharacter::DestroyDelegate, 20.f, false);
}

bool ACarditAICharacter::Killed_Validate(FPlayerInfo PlayerInfo)
{
	return true;
}

void ACarditAICharacter::DestroyDelegate()
{
	Destroy();
}