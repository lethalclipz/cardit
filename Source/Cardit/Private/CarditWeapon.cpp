// Fill out your copyright notice in the Description page of Project Settings.

#include "Cardit.h"
#include "CarditWeapon.h"
#include "CarditCharacter.h"
#include "UnrealNetwork.h"
#include "CarditGameMode.h"
#include "Engine.h"
#include "CarditAICharacter.h"
#include "CarditPlayerController.h"

// Sets default values for this component's properties
UCarditWeapon::UCarditWeapon()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	bReplicates = true;

	// ...
}


// Called when the game starts
void UCarditWeapon::BeginPlay()
{
	Super::BeginPlay();

	ReserveAmmo = StartingAmmo;

	Reload();
}

// Called every frame
void UCarditWeapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsReloading)
	{

	}
}

void UCarditWeapon::Fire()
{
	if (GetWorld()->GetTimeSeconds() - LastFireTime > GetTimeBetweenShotsSeconds() && !bIsReloading && AmmoInMagazine > 0)
	{
		ACarditCharacter* Owner = Cast<ACarditCharacter>(GetOwner());

		UCameraComponent* Camera = Owner->GetCamera();
		FVector CameraLocation = Camera->GetComponentLocation();
		FVector EndLocation = Camera->GetComponentLocation() + (Camera->GetComponentRotation().Vector() * MaxRangeInCm);

		// DrawDebugLine(GetWorld(), CameraLocation, EndLocation, FColor(255, 0, 0), false, 5.f, 0, 2.5f);

		FHitResult OutHit;
		FCollisionQueryParams Params;
		Params.AddIgnoredActor(Owner);
		
		// Play first person gunshot sound and explosion
		if (WeaponSound && GunshotExplosionParticleSystem)
		{
			UGameplayStatics::SpawnSoundAttached(WeaponSound, Owner->GetCurrentFirstPersonWeaponMesh(), FName("EndOfBarrel"));
			UGameplayStatics::SpawnEmitterAttached(GunshotExplosionParticleSystem, Owner->GetCurrentFirstPersonWeaponMesh(), FName("EndOfBarrel"));
		}

		FGunshotInfo ShotInfo;
		ShotInfo.Location = GetComponentLocation();
		ShotInfo.ParticleEffectLocation = Owner->GetCurrentThirdPersonWeapon()->GetSocketLocation(FName("EndOfBarrel"));
		ShotInfo.ParticleEffect = GunshotExplosionParticleSystem;
		ShotInfo.Sound = WeaponSound;
		ShotInfo.SoundAttenuationSettings = SoundAttenuationSettings;
		ShotInfo.CallingCharacterID = Owner->GetCharacterID();

		Owner->FireServerSide(ShotInfo);

		if (GetWorld()->LineTraceSingleByChannel(
			OutHit,
			CameraLocation,
			EndLocation,
			ECC_Visibility,
			Params
		)
			)
		{
			if (Cast<ACarditAICharacter>(OutHit.GetActor()))
			{
				if (Cast<ACarditAICharacter>(OutHit.GetActor())->IsDamageable())
				{
					DamageCharacter(Cast<ACarditAICharacter>(OutHit.GetActor()), DamagePerShot, Cast<ACarditPlayerController>(Owner->GetController())->GetPlayerInfo());

					// Display hitmarker
					Cast<ACarditPlayerController>(Owner->GetController())->DrawHitmarker();
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Hit %s"), *OutHit.GetActor()->GetName());
			}
		}

		Owner->AddRecoil(HorizontalRecoilFactor, VerticalRecoilFactor);

		Owner->PlayFireMontage();

		AmmoInMagazine--;

		LastFireTime = GetWorld()->GetTimeSeconds();
	}
	else if (AmmoInMagazine == 0)
	{
		RequestReload();
	}
}

void UCarditWeapon::RequestReload()
{
	if (ReserveAmmo > 0 && AmmoInMagazine < MagazineSize && !bIsReloading)
	{
		bIsReloading = true;
		GetWorld()->GetTimerManager().SetTimer(ReloadTimer, this, &UCarditWeapon::Reload, ReloadTimeSeconds, false);
	}
}

void UCarditWeapon::Reload()
{
	if (ReserveAmmo > 0 && AmmoInMagazine < MagazineSize)
	{
		int32 AmmoNeeded = MagazineSize - AmmoInMagazine;
		int32 AmmoTaken;
		if (ReserveAmmo >= AmmoNeeded)
		{
			ReserveAmmo -= AmmoNeeded;
			AmmoTaken = AmmoNeeded;
		}
		else
		{
			AmmoTaken = ReserveAmmo;
			ReserveAmmo -= AmmoTaken;
		}
		AmmoInMagazine += AmmoTaken;

		bIsReloading = false;
	}
}

void UCarditWeapon::DamageCharacter_Implementation(ACarditCharacter* CharacterToDamage, float Damage, const FPlayerInfo& PlayerInfo)
{
	if (CharacterToDamage->IsDamageable())
	{
		CharacterToDamage->DealDamage(Damage, PlayerInfo);
	}
}

bool UCarditWeapon::DamageCharacter_Validate(ACarditCharacter* CharacterToDamage, float Damage, const FPlayerInfo& PlayerInfo)
{
	return true;
}

void UCarditWeapon::AddAmmo_Implementation(int32 Amount)
{
	ReserveAmmo += Amount;
}

bool UCarditWeapon::AddAmmo_Validate(int32 Amount)
{
	return true;
}